
# Concluindo
Bom esse material chegou ao fim.  
Mas calma ele será atualizado de acordo com o tempo e caso você seja usuário assíduo do Metabase e queira compartilhar seu conhecimento e ajudar a manter atuaizado este documento fique a vontade.   

Toda ajuda é bem vinda.  

O intuito é manter este material gratuito.


Caso queira pode entrar em contato comigo via [linkedin](https://br.linkedin.com/in/diogo-miyake), ou [github](https://github.com/Miyake-Diogo).

Caso colaborou com atualização ou conteudos coloque seu contato aqui ... 


